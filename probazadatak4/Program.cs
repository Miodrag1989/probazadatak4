﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace probazadatak4
{
    class Program
    {
        static void Main(string[] args)
        {
            string recenica = Console.ReadLine();
            string[] reci = recenica.Split();
            while(recenica != ".")
            {
                int br = 0;
                foreach(string rec in reci)
                {
                    br++;
                }
                Console.WriteLine($"Uneta recenica sadrzi {br} reci");
                recenica = Console.ReadLine();
                reci = recenica.Split();
            }
        }
    }
}
